<?php

namespace Drupal\dwr\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeWidgetBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class implementation for the 'date_week_range' widget.
 *
 * @FieldWidget(
 *   id = "date_week_range",
 *   label = @Translation("Date and time week range"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateWeekRangeWidget extends DateRangeWidgetBase {

  /**
   * The date format storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityStorageInterface $date_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->dateStorage = $date_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('date_format')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_name = $items->getFieldDefinition()->getName();
    $statusId = Html::getId("edit-$field_name-$delta-status");

    // Identify the type of date and time elements to use.
    switch ($this->getFieldSetting('datetime_type')) {
      case DateRangeItem::DATETIME_TYPE_DATE:
      case DateRangeItem::DATETIME_TYPE_ALLDAY:
        $date_type = 'date';
        $time_type = 'none';
        $date_format = $this->dateStorage->load('html_date')->getPattern();
        $time_format = '';
        break;

      default:
        $date_type = 'date';
        $time_type = 'time';
        $date_format = $this->dateStorage->load('html_date')->getPattern();
        $time_format = $this->dateStorage->load('html_time')->getPattern();
        break;
    }

    $textfield = [
      '#type' => 'textfield',
      '#size' => 15,
      '#date_date_format' => $date_format,
      '#date_date_element' => $date_type,
      '#date_date_callbacks' => [],
      '#date_time_format' => $time_format,
      '#date_time_element' => $time_type,
      '#date_time_callbacks' => [],
    ];

    $element['datepicker'] = [
      '#markup' => '<div class="week-picker"></div><span id="' . $statusId . '"></span>',
      '#weight' => -1,
      '#attached' => [
        'library' => [
          'dwr/dwr',
        ],
        'drupalSettings' => [
          'date_week_range' => [
            'fieldName' => Html::getId("edit-$field_name-$delta"),
            'valueFieldName' => Html::getId("edit-$field_name-$delta-value"),
            'endValueFieldName' => Html::getId("edit-$field_name-$delta-end_value"),
            'statusID' => $statusId,
            'weekStart' => $this->getSetting('week_start'),
          ],
        ],
      ],
    ];

    $element['value'] = $textfield + $element['value'];
    $element['end_value'] = $textfield + $element['end_value'];

    if ($items[$delta]->start_date) {
      /** @var \Drupal\Core\Datetime\DrupalDateTime $start_date */
      $start_date = $items[$delta]->start_date;
      $element['value']['#default_value'] = $start_date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);
    }

    if ($items[$delta]->end_date) {
      /** @var \Drupal\Core\Datetime\DrupalDateTime $end_date */
      $end_date = $items[$delta]->end_date;
      $element['end_value']['#default_value'] = $end_date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);
    }

    $element['#element_validate'][] = [$this, 'validateWeekRange'];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateStartEnd(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // Overwrite the original validation because the values will come
    // parsed in a different way.
    $timezone = !empty($element['#date_timezone']) ? $element['#date_timezone'] : NULL;
    $start_date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $element['value']['#value'], $timezone);
    $end_date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $element['end_value']['#value'], $timezone);

    if ($start_date instanceof DrupalDateTime && $end_date instanceof DrupalDateTime) {
      if ($start_date->getTimestamp() !== $end_date->getTimestamp()) {
        $interval = $start_date->diff($end_date);
        if ($interval->invert === 1) {
          $form_state->setError($element, $this->t('The @title end date cannot be before the start date', ['@title' => $element['#title']]));
        }
      }
    }
  }

  /**
   * Ensure that the date range is a week.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateWeekRange(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $timezone = !empty($element['#date_timezone']) ? $element['#date_timezone'] : NULL;
    $start_date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $element['value']['#value'], $timezone);
    $end_date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $element['end_value']['#value'], $timezone);

    if ($start_date instanceof DrupalDateTime && $end_date instanceof DrupalDateTime) {
      $interval = $start_date->diff($end_date);
      if ($interval->days != 6) {
        $form_state->setError($element, $this->t("@title: The selected dates don't compose a week.", ['@title' => $element['#title']]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Ensure that the widget form element type has transformed the value to a
    // DrupalDateTime object at this point.
    foreach ($values as &$item) {
      if (!empty($item['value'])) {
        if (!$item['value'] instanceof DrupalDateTime) {
          $item['value'] = new DrupalDateTime($item['value']);
        }
      }

      if (!empty($item['end_value'])) {
        if (!$item['end_value'] instanceof DrupalDateTime) {
          $item['end_value'] = new DrupalDateTime($item['end_value']);
        }
      }
    }

    return parent::massageFormValues($values, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'week_start' => 1,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['week_start'] = [
      '#type' => 'select',
      '#title' => $this->t('Week starts on'),
      '#default_value' => $this->getSetting('week_start'),
      '#required' => TRUE,
      '#options' => array_reduce(range(0, 6), function($carry, $item) {
        $carry[$item] = $this->t(date('l', strtotime("Sunday +{$item} days")));
        return $carry;
      }, []),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $week_start = $this->getSetting('week_start');
    $summary[] = $this->t('Week starts on: @week_start', [
      '@week_start' => $this->t(date('l', strtotime("Sunday +{$week_start} days"))),
    ]);

    return $summary;
  }
}
