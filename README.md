Date Week Range field
=====================

Contents:
 * Introduction
 * History and Maintainers
 * Installation
 * Configuration
 * Support

Introduction
------------

The Date Week Range module provides a date range field widget that extends
the core _date range field_ module, and allows to select dates by week.

History and Maintainers
-----------------------

This module was created due a punctual need for a project at Cocomore AG,
originally written by Javier Díaz, who is the current maintainer.

Installation
------------

Date Week Range can be installed like any other Drupal 8 or 9 module and requires
no configuration prior to use.

1. Download the module to your DRUPAL_ROOT/modules directory, or wherever your contrib
modules are installed on the site.
2. Go to Admin > Extend and enable the module.

Configuration
-------------

The module doesn't need any configuration.

Support
-------

If you experience a problem with this module or have a problem, file a request
or issue on the flag queue at http://drupal.org/project/issues/dwr.

DO NOT POST IN THE FORUMS.

Posting in the issue queues is a direct line of communication with the module
authors.

No guarantee is provided with this software, no matter how critical your
information, module authors are not responsible for damage caused by this
software or obligated in any way to correct problems you may experience.

Licensed under the GPL 2.0.
http://www.gnu.org/licenses/gpl-2.0.txt
